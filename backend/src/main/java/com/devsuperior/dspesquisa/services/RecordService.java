package com.devsuperior.dspesquisa.services;

import com.devsuperior.dspesquisa.dto.RecordDTO;
import com.devsuperior.dspesquisa.dto.RecordInsertDTO;
import com.devsuperior.dspesquisa.entities.Game;
import com.devsuperior.dspesquisa.entities.Record;
import com.devsuperior.dspesquisa.repositories.GameRepository;
import com.devsuperior.dspesquisa.repositories.RecordRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RecordService {
    private final RecordRepository recordRepository;
    private final GameRepository gameRepository;
    private final ModelMapper mapper;

    public RecordService(RecordRepository recordRepository, GameRepository gameRepository, ModelMapper mapper) {
        this.recordRepository = recordRepository;
        this.gameRepository = gameRepository;
        this.mapper = mapper;
    }


    @Transactional
    public RecordDTO insert(RecordInsertDTO insertDTO) {
        Record entity = mapper.map(insertDTO, Record.class);

        entity.setMoment(Instant.now());

        Game game = gameRepository.getOne(insertDTO.getGameId());
        entity.setGame(game);

        //entity.getGame().getGenre().getName();

        entity = recordRepository.save(entity);

        return mapper.map(entity, RecordDTO.class);
    }

    @Transactional(readOnly = true)
    public Page<RecordDTO> findByMoments(Instant minDate, Instant maxDate, PageRequest pageRequest) {
        //Page<Record> page = recordRepository.findAll(pageRequest);
        Page<Record> page =recordRepository.findByMoments(minDate, maxDate, pageRequest);

        return page.map(x -> mapper.map(x, RecordDTO.class));
    }
}
