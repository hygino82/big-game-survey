package com.devsuperior.dspesquisa.services;

import com.devsuperior.dspesquisa.dto.GameDTO;
import com.devsuperior.dspesquisa.entities.Game;
import com.devsuperior.dspesquisa.repositories.GameRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class GameService {
    private final GameRepository gameRepository;
    private final ModelMapper mapper;

    public GameService(GameRepository gameRepository, ModelMapper mapper) {
        this.gameRepository = gameRepository;
        this.mapper = mapper;
    }

    @Transactional(readOnly = true)
    public Page<GameDTO> findAll(Pageable pageable) {
        Page<Game> page = gameRepository.findAll(pageable);

        return page.map(x -> mapper.map(x, GameDTO.class));
    }
}
