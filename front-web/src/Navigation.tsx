import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Header from './components/Header';
import Charts from './pages/Charts';
import Home from './pages/Home';
import Records from './pages/Records';

const Navigation = () => (
    <BrowserRouter>
        <Header />
        <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/records' element={<Records />} />
            <Route path='/charts' element={<Charts />} />
        </Routes>
    </BrowserRouter>
);


export default Navigation;